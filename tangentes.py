from decimal import Decimal
import math
import ujson as json
import matplotlib.pyplot as plt
from plot_figures import draw_circles, draw_trapezoid

class Circle:
	_pi=Decimal(math.pi)
	_fill='red'
	_opacity=0.5
	
	def __init__(self, x, y, radius):
		self._x = Decimal(x)
		self._y = Decimal(y)
		self._r = Decimal(radius)
		
	def set_radius(self, radius):
		self._r = Decimal(radius)
		
	def transform(self, x,y):
		self._x +=x
		self._y +=y
		
	def set_styles(self, color, opacity):
		self._fill=color
		self._opacity=opacity
		
	@property
	def x(self):
		return self._x
		
	@property
	def y(self):
		return self._y
		
	@property
	def position(self):
		return self.x, self.y
		
	@property
	def radius(self):
		return self._r
		
	@property
	def color(self):
		return self._fill

	@property
	def opacity(self):
		return self._opacity
		
	def __str__(self):
		return "Círculo de radio:%f, posicion (x,y)=(%f, %f)" %(
			self._r, self._x, self._y)
	
	def __repr__(self):
		return json.dumps(self.__dict__	)
		
	@property
	def area(self):
		return self._pi*self._r**2

	@property
	def perimeter(self):
		return 2*self._pi*self._r	
		
	@property
	def json(self):
		return {'x':self.x, 'y':self.y, 'r':self.radius, 'color':self.color}
		
	def __eq__(self, C1):
		return selc.radius==C1.radius	
	
	def __lt_(self, C1):
		return self.radius < C1.radius
		
	def __le__(self, C1):
		return self.radius <= C1.radius
		
	def __gt__(self, C1):
		return self.radius > C1.radius
		
	def __gt__(self, C1):
		return self.radius >= C1.radius
		
		
def distance(C0, C1):
	diff_y2 = Decimal(C0.y - C1.y)**2
	diff_x2 = Decimal(C0.x - C1.x)**2
	sqroot =  Decimal(math.sqrt(diff_y2+diff_x2))
	distance =  sqroot - C0.radius - C1.radius
	return distance
	
def posicion_y_tangente2circulos(C0, C1, distance):
	try:
		y=(C0.radius*(distance+2*C0.radius))/(C1.radius-C0.radius)
		return y		
	except Exception as e:
		print("Excepción de tipo %s" %e)
		raise e

def parametros_recta(C0, C1):
	diff_y = C1.y-C0.y
	diff_x = C1.x-C0.x
	if not diff_x == 0:
		m = diff_y/diff_x
		c = C1.y - m*C1.x
		return (m,c)
	else:
		return None, None
	
def recta(m,c,x):
	return m*x+c

def nueva_position(circle_list, m, w):
	C=min(circle_list)
	Cm=max(circle_list)
	if m:
		theta=math.atan(m)
		P2x=C.x-(C.radius+w)*math.cos(thetha)
		P2y=C.y-(C.radius+w)*math.sin(thetha)
		return P2x,P2y, theta	
	else:
		if C.y<=Cm.y:
			theta=0
			P2x=C.x
			P2y=C.y-(C.radius+w)
			return P2x,P2y, theta
		elif C.y>Cm.y:
			theta=0
			P2x=C.x
			P2y=C.y+(C.radius+w)
			return P2x,P2y, theta
		
	
def delta_ang(C, w, theta):
	if not theta==Decimal(math.pi/2):
		gamma = Decimal(math.acos(C.radius/(C.radius+w)))
		delta =  gamma - theta
		return delta
	else:
		return Decimal(math.acos(C.radius/(C.radius+w)))
	
def tang_points(C, delta, Cf):
	sign = 1
	if C.y > Cf.y:
		sign = -1
	T0 = (C.x+sign*C.radius*Decimal(math.sin(delta)),
		C.y+sign*C.radius*Decimal(math.cos(delta)))
	T1 = (C.x-sign*C.radius*Decimal(math.sin(delta)),
		C.y+sign*C.radius*Decimal(math.cos(delta)))
	return T0, T1
	
def generate_trapezoid_svg(T00,T01,T11,T10, color='red'):
	lista_str=["%f,%f" %(x,y) for x,y in (T00,T01,T11,T10)]
	points = " ".join(lista_str)
	stroke_w="stroke-width:%s" %0
	stroke_c="stroke:%s" %color
	values={"points":points, "color":color, "stroke-width":stroke_w,  "stroke":stroke_c}
	svg="<polygon points=\"{points}\" style=\"{stroke}{stroke-width}\" fill=\"{color}\"/>".format(**values)
	return svg
	
def generate_circle_svg(C):
	template_svg="""
	<circle
       style="opacity:1;fill:{color};fill-opacity:1;stroke:none;stroke-width:0;"
       id="path12"
       cx="{x}"
       cy="{y}"
       r="{r}" />"""
	svg = template_svg.format(**C.json)
	return svg	
	
if __name__ == "__main__":
	# circulo crande
	R=40
	p0=[48.012302, 43.550255]
	r=10
	# circulo menor
	p1=[48.012302, 113.55025]
	C0 = Circle(*p0,R)
	C0.set_styles('blue', .7)
	C1 = Circle(*p1,r)
	C1.set_styles('green', .4)
	print(C0, C1, "Circulo mayor", max([C0,C1]))
	circle_list = sorted([C0,C1])
	print(circle_list)
	D=distance(*circle_list)
	print("Distancia entre círculos ", D)
	w=posicion_y_tangente2circulos(*circle_list,D)
	print("Distancia extra a punto de fuga w",w)
	m, c = parametros_recta(*circle_list)
	print("Parámetros recta", m,c )
	# calcular los nuevos puntos, donde partir la recta tangente
	C2x,C2y,theta=nueva_position(circle_list, m, w)
	print("Punto de fuga", C2x, C2y, theta)
	delta = delta_ang(min(circle_list),w,theta)
	print("Angulo delta -> %s rads" %delta)
	C2 = Circle(C2x,C2y,1)
	T00, T01 =  tang_points(min(circle_list), delta, C2)
	T10, T11 =  tang_points(max(circle_list), delta, C2)
	print("Tamaño y -> %s" %w)
	print("Posicion punto escape P3-> %s, %s" %(C2x,C2y))
	print("Los puntos tangentes al círculo mayor son: %s, %s" %(T10, T11))
	print("Los puntos tangentes al círculo menor son: %s, %s" %(T00, T01))
	## Dibujando figuras
	fig, ax = plt.subplots(figsize=(80, 80))
	ax.set_xlim((0,150))
	ax.set_ylim((0,150))
	circle_list.append(C2)
	draw_circles(circle_list, ax)
	draw_trapezoid(T00,T01,T11,T10, ax)	
	svg_t=generate_trapezoid_svg(T00,T01,T11,T10)
	print("==Trapezoide==")
	print(svg_t)
	svg_c0=generate_circle_svg(C0)
	svg_c1=generate_circle_svg(C1)
	print("Circle 0 svg: ", svg_c0)
	print("Circle 1 svg: ", svg_c1)		
	print("==TO svg:::====")
	print(svg_c0)
	print(svg_c1)
	print(svg_t)
	plt.show()
	
