import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, PathPatch
from matplotlib.path import Path
from matplotlib.transforms import Affine2D
from matplotlib.patches import Wedge, Polygon

import numpy as np


def draw_circles(circle_list, ax):
	for circle in circle_list:
		circle_fig = Circle(list(map(float, circle.position)), 
						float(circle.radius), 
						facecolor=circle.color, 
						edgecolor=(0,0.8,0.8),
						linewidth=0,
						alpha=circle.opacity)
		ax.add_patch(circle_fig)
		

def draw_trapezoid(T00,T01,T10,T11, ax):
	"""
	Dibuja un trapecio
	"""
	print(T00,T01,T10,T11)
	q=[	list(map(float,T00)),
		list(map(float,T01)),
		list(map(float,T10)),
		list(map(float,T11))]
	[print(e) for e in q]
	np_tangent_points=np.array(q)
	polygon = Polygon(np_tangent_points)
	ax.add_patch(polygon)
	
	
	
